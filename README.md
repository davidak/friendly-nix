# friendly-nix

A user-friendly wrapper around Nix.

## Why this is needed

I'm frustrated with the Nix UI for years and don't feel my concerns and suggestions are heared.

I often stated that simple tasks should be able to do with simple commands, like `nix install firefox` to install the firefox package from the default (stable) channel.
Sure, Nix is very powerfull and can do a lot of complicated things, but why make basic tasks also complicated?

The new (experimental) UI of Nix to do that looks like this: `nix profile install nixpkgs#firefox`

I will add more features and criticism later.

So the goal of this project for me is to stop complaining and do something myself. Maybe it will get popular and my suggestions are considered then.
I usually try to contribute upstream (and i did, often), but this time i do my own project how i think it's best.

I started this project in 2022 when i was again arguing over the same points again (in [nix issue 6475](https://github.com/NixOS/nix/issues/6475#issuecomment-1119174082)).

## License

The code is licensed under the MIT license to be compatible with the ecosystem this project operates in.
